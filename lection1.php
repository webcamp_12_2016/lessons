<?php

    class Human {

        public $sound = 'normal';

        protected $brain = 'normal';

        private $heart = 'good';

        public function setBrain($brain)
        {
            $this->brain = $brain;
        }

        public function getBrain()
        {
            return $this->brain;
        }

        public function showKnowledge()
        {
            echo "My brain {$this->brain}\n";
        }

        public function sound()
        {
            echo "Sound {$this->sound}\n";
        }

        public function showHealth()
        {
            echo "My heart is {$this->heart}\n";
        }
    }

    class Person extends Human
    {
        protected $name;
        private $heart = 'cool';

        public function showPersonHealth()
        {
            echo "My person heart is {$this->heart}\n";
        }


        public function showKnowledge()
        {
            echo "My person brain {$this->brain}\n";
        }
    }

    class Student extends Person
    {
        private $heart = 'bad';

        protected $brain = 'clever';

        public function showStudentHealth()
        {
            echo "My person heart is {$this->heart}\n";
        }

        public function showKnowledge()
        {
            echo "My student brain {$this->brain}\n";
        }
    }

    $human = new Human();

    var_dump($human);
    print_r($human);

    $human->sound = 'soft';
//    $human->brain = 'clever'; // Fatal error
    $human->setBrain('clever');
    $human->sound();
    $human->showKnowledge();

    $newHuman = new Human();
    $newHuman->sound = 'rude';
    $newHuman->sound();
    $newHuman->showKnowledge();
    $newHuman->showHealth();


    $person = new Person();
    $person->showHealth();
    $person->showPersonHealth();
    $person->showKnowledge();

    var_dump($person);
    print_r($person);

    $student = new Student();
//    $student->setBrain('good');
    $student->showHealth();
    $student->showPersonHealth();
    $student->showStudentHealth();
    $student->showKnowledge();

    var_dump($student);
    print_r($student);


    function showKnowledge(Person $h) {
        $h->showKnowledge();
    }

    echo "SHOW KNOWLEDGE\n";

    $humans = [
        $human,
        $newHuman,
        $person,
        $student
    ];

    foreach ($humans as $human)
    {
        $className = get_class($human);
        if($human instanceof Person) {
            echo "This {$className} is Person\n";
            showKnowledge($human);
        } else {
            echo "This {$className} is not Person\n";

        }
    }



    // static


    class Greeting
    {
        const MALE = 'male';
        const FEMALE = 'female';

        public static function getGenderList()
        {
            return [
                self::MALE,
                self::FEMALE,
            ];
        }

        static public $lastName;

        static public function hello($name)
        {
            echo "Hello {$name}\n";
            self::$lastName = $name;
        }

        static public function helloToLastName()
        {
            echo "Hello ".self::$lastName."\n";
        }

    }

    Greeting::$lastName = 'Hello';
    Greeting::hello('Artem');
    Greeting::helloToLastName();
    Greeting::helloToLastName();




















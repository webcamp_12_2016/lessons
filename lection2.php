<?php

    class Animal {

        public $sound = '';
        public $pawsCount = 0;

        public function sound()
        {
            echo "Animal produce sound {$this->sound}\n";
        }

        public function makeAStep()
        {
            echo "Animal make step with {$this->pawsCount} paws\n";
        }

        public function __construct()
        {
            echo "Object Animal Created\n";
        }
    }

    class Cat extends Animal {

        public $sound = 'meay';
        public $pawsCount = 4;

    }

    class Pet extends Cat
    {

        private $name;

        private $lastName = 'Sir';

        /**
         * Control create object
         * @param $name
         */
        public function __construct($name)
        {
            $this->name = $name;
        }

        public function __destruct()
        {
            echo "Your pet {$this->name} not available now\n";
        }

        public function call()
        {
            echo "Come to me my {$this->name}\n";
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }
//
//        /**
//         * @param mixed $name
//         */
//        public function setName($name)
//        {
//            $this->name = $name;
//        }

        public  function __call($name, $arguments)
        {
//            echo "__Call\n";
//            var_dump($name);
//            var_dump($arguments);

            if(false !== strpos($name,'get')) {
                $paramName = lcfirst(substr($name,3));
                return $this->{$paramName};
            }
            if(false !== strpos($name,'set')) {
                $paramName = lcfirst(substr($name,3));
                $this->{$paramName} = $arguments[0];
                return $this;
            }
        }
    }

    class Address
    {
        private $_data = [];

        public function __construct(array $data)
        {
            $this->_data = $data;
        }

        public  function __call($name, $arguments)
        {
            if(false !== strpos($name,'get')) {
                $paramName = lcfirst(substr($name,3));
                return $this->_data[$paramName];
            }

            if(false !== strpos($name,'set')) {
                $paramName = lcfirst(substr($name,3));
                $this->_data[$paramName] = $arguments[0];
                return $this;
            }
        }

        public function __callStatic($name, $args)
        {
            echo "__CallStatic\n";
            var_dump($name);
            var_dump($args);
        }

        public function __get($name)
        {
            echo "__get\n";
            return $this->_data[$name];
        }

        public function __set($name,$value)
        {
            echo "__set\n";
            $this->_data[$name] = $value;
        }

        public function __isset($name)
        {
            echo "__isset\n";
            return array_key_exists($name,$this->_data);
        }

        public function __unset($name)
        {
            unset($this->_data[$name]);
        }

        public function __wakeup()
        {
            echo "__wakeup\n";
        }

        public function __sleep()
        {
            echo "__sleep\n";
            return ['_data'];
        }

        public function __toString()
        {
            return implode('',$this->_data)."\n";
        }

        public function __invoke($param = 1)
        {
            echo "__invoke\n";
            echo "Param -> $param";
            return $this->_data;
        }

        public function __set_state()
        {
            // for var_export
            return [];
        }

        public function __debugInfo()
        {
            return [
                'test12' => 123
            ];
        }

        public function __clone()
        {
            echo "__clone";
            // TODO: Implement __clone() method.
        }
    }

    $cat = new Cat();
    $cat->makeAStep();
//    $cat = new Animal;
//    $cat->pawsCount = 4;
//    $cat->sound = 'meay';


    var_dump($cat);

    $pet = new Pet("Micky");
    $pet->call();

    var_dump($pet);

    unset($pet);

    $spot = new Pet("Spot");
    $spot->call();

    var_dump($spot->getName());
    var_dump($spot->getLastName());
    $spot
        ->setName('Test')
        ->setLastName('The Great');

    var_dump($spot->getName());
    var_dump($spot->getLastName());

    var_dump($spot);

    $addr = new Address([
        'index' => '123456',
        'street' => 'Baker street',
        'city' => 'London',
    ]);

    var_dump($addr->getCity());
    var_dump(Address::getCity());

    var_dump($addr->city);
    $addr->city = 'New York';
    var_dump($addr->city);
    var_dump(isset($addr->city));
    unset($addr->city);
//    var_dump($addr->city);
    var_dump($addr);

    $seralizedAddr = serialize($addr);
    var_dump($seralizedAddr);
    $objAddr = unserialize($seralizedAddr);
    var_dump($objAddr);
    echo $objAddr;

    var_dump($objAddr());

    var_export($objAddr,false);


    var_dump($addr);
    $addrLink = $addr;
    $addrLink->setCity('Kiev');
    var_dump($addrLink);
    var_dump($addr);


    $addrClone = clone $addr;
    $addrClone->setCity('Monreal');
    var_dump($addrClone);
    var_dump($addr);








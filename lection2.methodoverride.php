<?php

    abstract class Animal {

        public $sound = '';
        public $pawsCount = 0;

        public function sound()
        {
            echo "Animal produce sound {$this->sound}\n";
        }

        public function makeAStep()
        {
            echo "Animal make step with {$this->pawsCount} paws\n";
        }

        public function __construct()
        {
            echo "Object Animal Created\n";
        }

        abstract public function getType();
    }

    interface PlayingInterface {
//        const TEST = 'test';
        public function play($toyName);
    }

    trait PayingTrait
    {
        public function play($toyName)
        {
            echo "Animal play with {$toyName}\n";
        }
    }

    class Cat extends Animal implements PlayingInterface {

        use PayingTrait;

        public $sound = 'meay';
        public $pawsCount = 4;

//        public function play($toyName)
//        {
//            echo "Cat play with {$toyName}\n";
//        }

        public function getType()
        {
            return "Animal is Cat";
        }
    }

    class Dog extends Animal implements PlayingInterface {

        use PayingTrait;

        public $sound = 'bow';
        public $pawsCount = 4;

//        public function play($toyName)
//        {
//            echo "Dog play with {$toyName}\n";
//        }

        public function getType()
        {
            return "Animal is Dog";
        }
    }

    class Snake extends Animal {
        public $sound = 'sss';
        public $pawsCount = 0;

        public function getType()
        {
            return "Animal is Snake";
        }
    }

    class Pet extends Cat
    {

        private $name;

        private $lastName = 'Sir';

        /**
         * Control create object
         * @param $name
         */
        public function __construct($name)
        {
            $this->name = $name;
        }

        public function __destruct()
        {
            echo "Your pet {$this->name} not available now\n";
        }

        public function call()
        {
            echo "Come to me my {$this->name}\n";
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @param $toyName
         */
        public function play($toyName)
        {
            echo "{$this->name} play with {$toyName} \n";
        }

        /**
         * wrong override
         */
//        public function play($toy,$count)
//        public function play(Toy $toy)
        public function playWithToy(Toy $toy)
        {
            echo "{$this->name} play with {$toy->getName()} \n";
        }

    }

    class Toy
    {
        protected $name;

        protected $price;

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @return mixed
         */
        public function getPrice()
        {
            return $this->price;
        }

    }

    $cat = new Cat();
    $dog = new Dog();
    $snake = new Snake();

    $animals = [
        $cat,
        $dog,
        $snake,
    ];

    foreach ($animals as $animal)
    {
        if($animal instanceof PlayingInterface
        )
        {
            $animal->play('ball');
        }
    }



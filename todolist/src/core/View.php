<?php

namespace core;


class View implements ViewInterface
{
    protected $viewPath = 'views';
    protected $layoutPath = 'layouts';

    /** @var Controller */
    protected $controller;
    /** @var Application */
    protected $app;

    public function render($viewName, $data)
    {
//        $viewPath = $this->app->getAppPath()
        $viewPath = Application::getAppPath()
            .DIRECTORY_SEPARATOR.'src'
            .DIRECTORY_SEPARATOR.$this->viewPath
            .DIRECTORY_SEPARATOR.$this->getController()->getControllerName()
            .DIRECTORY_SEPARATOR.$viewName.'.php';

        $layoutPath = Application::getAppPath()
            .DIRECTORY_SEPARATOR.'src'
            .DIRECTORY_SEPARATOR.$this->layoutPath
            .DIRECTORY_SEPARATOR.$this->getController()->getLayoutName().'.php';

        if (!file_exists($layoutPath)) {
            throw new \Exception('No layout "'.$this->getController()->getLayoutName().'" available!');
        }

        if (!file_exists($viewPath)) {
            throw new \Exception('No view "'.$viewName.'" available!');
        }

        if(array_key_exists('content',$data)) {
            throw new \Exception('Invalid key "content" for data');
        }

        if (is_array($data)) {
            extract($data, EXTR_SKIP);
        }

        ob_start();
        include($viewPath);
        $content = ob_get_clean();

        ob_start();
        include($layoutPath);
        $fullContent = ob_get_clean();

        ob_end_clean();

        echo $fullContent;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function setController(Controller $controller)
    {
        $this->controller = $controller;
        return $this;
    }

    public function getSession()
    {
        return Application::getSession();
    }

}
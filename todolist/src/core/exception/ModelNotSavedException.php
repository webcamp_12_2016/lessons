<?php

namespace core\exception;


use Exception;

class ModelNotSavedException extends DbException
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
        parent::__construct();
    }

    public function getModel()
    {
        return $this->model;
    }
}
<?php

namespace core\exception;

class PageNotFoundException extends HttpException
{
    public function __construct($message = 'Page not found')
    {
        parent::__construct(404,$message);
    }
}
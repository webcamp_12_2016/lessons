<?php

namespace core\exception;

class HttpException extends BaseException
{
    const SERVER_ERROR_CODE = 500;

    protected $httpCode;

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function __construct($httpCode,$message = "")
    {
        $this->httpCode = $httpCode;
        parent::__construct($message);
    }
}
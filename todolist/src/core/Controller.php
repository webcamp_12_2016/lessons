<?php
namespace core;

abstract class Controller
{
    protected $layout = 'main';

    public function runAction($action)
    {
        $actionMethodName = 'action'.ucfirst($action);

        if (!method_exists($this, $actionMethodName)) {
            throw new \Exception(
                'Controller class '.get_class($this)
                .' dont have action '.$actionMethodName.'!'
            );
        }

        return $this->{$actionMethodName}();
    }

    public function getControllerName()
    {
        $controllerClassName = get_class($this);
        $pos = strripos($controllerClassName,'\\');
        if(false !== $pos) {
            $controllerClassName =
                substr($controllerClassName,$pos+1);
        }
        $name = str_ireplace('controller','',$controllerClassName);

        return lcfirst($name);
    }

    protected function render($name,$data)
    {
        $this->getView()->render($name,$data);
    }

    protected function getView()
    {
        /**
         * @todo: move to container
         */
        $view = new View();
        $view->setController($this);

        return $view;
    }

    /**
     * @return string
     */
    public function getLayoutName()
    {
        return $this->layout;
    }


    public function redirect()
    {
        header('Location: / ');
        die();
    }

}
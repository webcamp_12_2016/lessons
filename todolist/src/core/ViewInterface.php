<?php
namespace core;

interface ViewInterface
{
    public function render($name,$data);
}
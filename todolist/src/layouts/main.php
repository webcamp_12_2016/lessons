<!DOCTYPE html>
<html>
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $this->pageTitle; ?></title>
        <link rel="stylesheet" href="/public/css/style.css">
    </head>
<body>
<div>
    <div>
        <h1>Todo list</h1>
    </div>
    <div>
        <?php
            if($this->getSession()->isLoginned()) {
                echo 'Hello, '.$this->getSession()->getName();
                echo '<a href="/logout">Logout</a>';
            } else {
                echo 'Hello Guest';
                echo '<a href="/login">Login</a>';
            }
        ?>
    </div>
</div>
<div class="container">
    <?php echo $content; ?>
</div>

</body>
</html>
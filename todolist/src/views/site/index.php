<h2>Main page</h2>

<?php
    if(!empty($todos)) {
        ?>
        Todo
        <ul>
            <?php
                foreach ($todos as $todo) {
                    echo '<li>';
                    echo $todo->text;
                    $checked = \model\Todo::DONE === $todo->done ? 'checked="cheched"': '';
                    echo '<input '.$checked.' type="checkbox">';
                    echo '</li>';
                }
            ?>
        </ul>
        <?php
    }
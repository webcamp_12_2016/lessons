<?php

namespace model;

use core\Model;

class Todo extends Model
{
    const DONE = '1';
    const NOT_DONE = '0';

    public static function getTableName()
    {
        return 'todo';
    }

    public function getAttributeNames()
    {
        return [
            'todoId' => 'Todo ID',
            'userId' => 'User ID',
            'text' => 'Text',
            'done' => 'Done',
        ];
    }

    public function getPrimaryKeyName()
    {
        return 'todoId';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }
}
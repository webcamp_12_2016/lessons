<?php
namespace controller;

use core\Application;
use core\Controller;
use core\exception\ModelNotFoundException;
use core\exception\ModelNotSavedException;
use model\Todo;
use model\User;

class SiteController extends Controller
{
//    protected $layout = 'site';

    protected function actionIndex()
    {
        $data = [];
        if(Application::getSession()->isLoginned()) {
            $data['todos'] = Todo::find('`userId` = :uId',
                [
                    ':uId' => Application::getSession()->getId()
                ]);
        }
        $this->render('index',$data);
    }

    protected function actionLogin()
    {
        if(Application::getSession()->isLoginned()) {
            $this->redirect('/');
        }

        $errors = [];
        if([] !== $_POST) {
            if(isset($_POST['email']) && isset($_POST['password'])) {
                $email = $_POST['email'];
                $password = $_POST['password'];
                try {
                    $user = User::findOne('email = :e',[':e' => $email]);
                    if(password_verify($password,$user->password)) {
                        Application::getSession()->login($user);
                        $this->redirect('/');
                    }
                } catch (ModelNotFoundException $e) {
                    echo "<h1>Ex</h1>";
                } finally {
                    $errors[] = 'Invalid email or password';
                }
            } else {
                $errors[] = 'Set all fields!';
            }
        }

        $this->render('login',[
//   for PHP7     'email' => ($_POST['email'] ?? ''),
                'email' => (isset($_POST['email']) ? $_POST['email']:''),
                'errors' => $errors
        ]);
    }

    public function actionLogout()
    {
        Application::getSession()->logout();
        $this->redirect('/');
    }

    public function actionRegister()
    {
        throw new \Exception('Not implemented!');
        //        $newUser = new User([
//                'name' => 'Andrew',
//                'email' => 'andrew@mailinator.com',
//                'password' => password_hash('andrew',PASSWORD_BCRYPT),
//
//        ]);
//
//        $newUser->save();
    }
}
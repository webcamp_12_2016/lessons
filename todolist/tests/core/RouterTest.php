<?php

namespace tests\core;

use core\Router;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    public function testRules()
    {
        $router = new Router('/login',[
            [
                'pattern'    => '/login',
                'controller' => 'site',
                'action'     => 'login',
            ],
        ]);

        $this->assertEquals('site',$router->getControllerName());
        $this->assertEquals('login',$router->getActionName());
    }

//    public function testInvalidRules()
//    {
//        $router = new Router('login',[
//            [
//                'pattern'    => '/login',
//                'controller' => 'site',
//                'action'     => 'login',
//            ],
//        ]);
//
//        $this->assertEquals('site',$router->getControllerName());
//        $this->assertEquals('login',$router->getActionName());
//    }
}
<?php
return [
    [
        'pattern'    => '/',
        'controller' => 'site',
        'action'     => 'index',
//                'method' => 'post',
    ],
    [
        'pattern'    => '/login',
        'controller' => 'site',
        'action'     => 'login',
//                'method' => 'post',
    ],
    [
        'pattern'    => '/logout',
        'controller' => 'site',
        'action'     => 'logout',
//                'method' => 'post',
    ],
    [
        'pattern'    => '/list',
        'controller' => 'list',
        'action'     => 'index',
    ],
];
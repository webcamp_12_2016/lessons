CREATE TABLE `user`
(
  `userId` INT(11) PRIMARY KEY AUTO_INCREMENT,
  `name` char(50),
  `email` char(100),
  `password` CHAR (255)
) ENGINE=InnoDb COLLATE 'utf8_general_ci';

CREATE TABLE `todo`
(
  `todoId` INT(11) PRIMARY KEY AUTO_INCREMENT,
  `userId` INT(11) DEFAULT NULL,
  `text` char(50),
  `done` int(1) DEFAULT 0,
  FOREIGN KEY `todo_user` (`userId`)
  REFERENCES `user`(`userId`)
    ON UPDATE CASCADE ON DELETE CASCADE

) ENGINE=InnoDb COLLATE 'utf8_general_ci';
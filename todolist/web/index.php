<?php
error_reporting(E_ALL);
ini_set('display_errors','On');

//require_once('../src/core/Autoloader.php');
require(__DIR__ . '/../vendor/autoload.php');

//use core\Application;
//$app = Application::getInstance();

$app = core\Application::getInstance();

$appMode = core\Application::MODE_DEV;
//$appMode = getenv('DEVMODE');
$app->run($appMode);
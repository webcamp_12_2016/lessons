<?php

abstract class Figure {
    abstract public function getPerimetr();
    abstract public function getArea();
}

class Rectangle extends Figure
{
    /**
     * @var float
     */
    protected  $height;

    /**
     * @var float
     */
    protected $width;

    public function __construct($height,$width)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function getPerimetr()
    {
        return ($this->height + $this->width) * 2;
    }

    public function getArea()
    {
        return $this->height * $this->width;
    }

}


class Square extends Rectangle  {

    public function __construct($side)
    {
        parent::__construct($side,$side);
    }
}


class Circle extends Figure {

    /**
     * @var float.
     */
    private $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function getPerimetr()
    {
        return 2 * pi() * $this->radius;
    }

    public function getArea()
    {
        return pi() * $this->radius * $this->radius;
    }

}


$c = new stdClass();
$c->testData = [1,2,3,4];

$objects = [
  new Square(3),
    new Rectangle(5,4),
    $c,
    new Circle(10),
];

foreach ($objects as $object)
{
    if($object instanceof Figure) {
        echo "Perimetr is ".$object->getPerimetr()."\n";
    }
}

var_dump($objects);

